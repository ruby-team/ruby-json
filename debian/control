Source: ruby-json
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Lucas Nussbaum <lucas@debian.org>,
           Cédric Boutillier <boutil@debian.org>,
           Utkarsh Gupta <utkarsh@debian.org>,
           Lucas Kanashiro <kanashiro@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 2.1~),
               ruby-test-unit,
               ruby-test-unit-ruby-core
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-json.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-json
Homepage: https://ruby.github.io/json
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-json
Architecture: any
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Conflicts: ruby-json-pure
Multi-Arch: same
Description: JSON library for Ruby
 This library implements the JSON (JavaScript Object Notation) specification in
 Ruby, allowing the developer to easily convert data between Ruby and JSON. You
 can think of it as a low fat alternative to XML, if you want to store data to
 disk or transmit it over a network rather than use a verbose markup language.
